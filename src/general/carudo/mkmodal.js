import React, { Component, useState } from 'react'
import PropTypes from 'prop-types'

import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Typography from '@material-ui/core/Typography'
import Chip from '@material-ui/core/Chip'
import { withStyles } from '@material-ui/core/styles'
import Avatar from '@material-ui/core/Avatar'
import brown from '@material-ui/core/colors/brown'
import MemoryIcon from '@material-ui/icons/Memory'
import Tooltip from '@material-ui/core/Tooltip'
import Switch from '@material-ui/core/Switch'
import FormControlLabel from '@material-ui/core/FormControlLabel'

import { Grid, Row, Col } from 'react-flexbox-grid'

import { When, Then, Else, If } from 'react-if'

import { observer, inject } from 'mobx-react'

import Carudo from "./carudo";
import SemesterChips from './SemesterChips'


const InfoChips = withStyles({
    root: {
      marginRight: 5,
      overflow: "visible"
    },
    colorPrimary: {
      backgroundColor: brown['800'],
      color: "#fff"
    }
  })(Chip);

const styles = {
    prasyaratContainer: {
        marginBottom: 20,
        marginTop: 10
    }
}


const PrasyaratLister = params => {

    const { datas, label, description, descriptionNone } = params;
    const [ showDetail, setShowDetail ] = useState(false);
    
    return <div style={{marginBottom: 30}}>
        <div style={{display:'flex', alignItems:'center', minHeight:48}}>
            <Typography variant="subtitle1"  style={{flexGrow:'1'}}>{label}</Typography>
            <When condition={datas.length > 0}>
                <FormControlLabel control={<Switch checked={showDetail} onChange={(e,val)=>{setShowDetail(val)}}/>} label="Detilkan"/>
            </When>
        </div>
        <If condition={datas !== undefined && datas.length > 0}>
            <Then>
                <Typography variant="subtitle2" style={{marginBottom: 10}}>{description}</Typography>
                <If condition={showDetail}>
                    <Then>
                        <Grid>
                            <Row>
                                {datas.map((da,i )=>
                                    <Col sm={6} key={i}>
                                        <Carudo data={da}/>
                                    </Col>
                                )}
                            </Row>
                        </Grid>
                    </Then>
                    <Else>
                        {datas.map((da,i )=>{
                            return <Tooltip title={da.nama + ", " + da.sks + " SKS."} key={i}>
                                <InfoChips label={da.kode} icon={da.wajib?<MemoryIcon />:''}/>
                            </Tooltip>
                        })}
                    </Else>
                </If>
            </Then>
            <Else>
                <Typography variant="subtitle2">{descriptionNone}</Typography>
            </Else>
        </If>
    </div>
}



class MKModal extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired
  }

  render() {
      const { classes, data, appstore } = this.props;

    return (
      <div>
        <Dialog open={data.showDetail} maxWidth="sm" onClose={()=>{data.showDetail = false}} fullWidth>
            <DialogTitle>Detil {data.kode} - {data.nama}</DialogTitle>
            <DialogContent>
                <div className={classes.prasyaratContainer}>
                    <InfoChips variant="outlined" label="sks" avatar={<Avatar>{data.sks}</Avatar>} />
                    <When condition={data.wajib}>
                        <Chip color="primary" variant="outlined" icon={<MemoryIcon />} label="Wajib" />
                    </When>
                    <SemesterChips semester={data.semester}/>
                </div>
                <PrasyaratLister 
                    datas={appstore.findDetail(data.prasyarat.tempuh)}
                    label="Prasyarat Tempuh" 
                    description="Anda harus menempuh mata kuliah berikut untuk dapat mengambil MK ini"
                    descriptionNone="MK ini tidak memiliki prasyarat tempuh."
                />
                <PrasyaratLister 
                    datas={appstore.findDetail(data.prasyarat.lulus)}
                    label="Prasyarat Lulus" 
                    description="Anda harus lulus (>E) mata kuliah berikut untuk dapat mengambil MK ini"
                    descriptionNone="MK ini tidak memiliki prasyarat lulus."
                />
                <PrasyaratLister 
                    datas={appstore.findDetail(data.prasyarat.bersamaan)}
                    label="Prasyarat Bersamaan" 
                    description="Mata kuliah ini harus diambil bersamaan dengan matakuliah berikut:"
                    descriptionNone="MK ini tidak memiliki prasyarat bersamaan apapun."
                />
                <div className={classes.prasyaratContainer}>
                    <When condition={data.prasyarat.berlakuAngkatan !== undefined && data.prasyarat.berlakuAngkatan !== null}>
                        <div style={{display:'flex', alignItems:'center', minHeight:48}}>
                            <Typography variant="subtitle1">Berlaku untuk angkatan</Typography>
                        </div>
                        <Typography variant="subtitle2" style={{marginBottom: 10}}>
                            Mata kuliah ini mulai berlaku sejak angkatan <b>{data.prasyarat.berlakuAngkatan}</b>.
                        </Typography>
                    </When>
                </div>
            </DialogContent>
        </Dialog>
      </div>
    )
  }
}

export default inject("appstore")(withStyles(styles)(observer(MKModal)));