import { observable, action } from 'mobx'
import axios from 'axios';
export default class AppStore {
    matakuliah = observable.array([], {
        deep:true
    })

    sortBySmester(data) {
        return data.sort((satu,dua)=> satu.semester - dua.semester);
    }

    loadMatakuliah = action(()=>{
        axios.get("https://ftisunpar.github.io/data/prasyarat.json")
            .then((data)=>{
                this.matakuliah.replace(this.sortBySmester(data.data).map(e=>{
                    e.showDetail = false;
                    return e;
                }));
            });
    })

    findDetail(arrayMK){
        if(arrayMK.length === 0)
            return [];
        return this.matakuliah.filter((e)=>arrayMK.includes(e.kode));
    }
}