import React, { Component } from 'react';
import NavAppBar from './general/navappbar';
import Index from './index/index';

import { Provider } from 'mobx-react';
import AppStore from './store/appstore';

class App extends Component {
  render() {
    return (
      <Provider appstore={new AppStore()}>
        <div style={{marginTop:64}}>
          <NavAppBar />
          <Index />
        </div>
      </Provider>
    );
  }
}

export default App;
